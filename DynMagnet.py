import sip
import sys


from PyQt5 import QtGui,QtCore,uic,QtWidgets
from PyQt5.QtWidgets import QMainWindow,QDoubleSpinBox
from PyQt5.uic import loadUiType



Ui_DynMagnetGUI, QMainWindow = loadUiType('DynMagnetGUI.ui')

class DynMagnet(QMainWindow,Ui_DynMagnetGUI):
    def __init__(self,):
        super(DynMagnet,self).__init__()
        self.setupUi(self)

        print('OrbitModel: Initializing CAFE...')

        self.cafe=PyCafe.CyCafe()
        self.cyca=PyCafe.CyCa()
        self.cafe.withExceptions(False)


        print('Building the Interface')

        self.savebutton={}
        self.recallbutton={}
        self.deletebutton={}
        self.SetValue={}
        self.RBValue={}
        self.group={}
        self.saveval={}


        self.SaveAll.clicked.connect(self.DoSaveAll)
        self.RestoreAll.clicked.connect(self.DoRestoreAll)
        self.Close.clicked.connect(self.GoInHibernation)

        self.timer=QtCore.QTimer(self)
        self.connect(self.timer,QtCore.SIGNAL("timeout()"),self.QTimerEvent)
        self.timer.start(1000)


#-------
# Initializing the panels

    def addMagnet(self,name):
  
        val=self.getMagnetSettings(name)
        if val is None:
            print('Problems with connecting to',name)
            return

        self.group[name]=QtGui.QGroupBox(name,self)
        layout=QtGui.QHBoxLayout(self.group[name])

#        self.SetValue[name]=QtGui.QDoubleSpinBox(self)
        self.SetValue[name]=custom_spinbox(self)
        self.SetValue[name].setDecimals(3)
        self.SetValue[name].setMaximum(200)
        self.SetValue[name].setMinimum(-200)
        self.SetValue[name].setValue(val[0])
        self.SetValue[name].setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.SetValue[name].setFixedWidth(90)
        self.SetValue[name].setObjectName(name)
        self.SetValue[name].valueChanged.connect(self.writeSV)
        self.RBValue[name]=QtGui.QLineEdit("0.001",self)
        self.RBValue[name].setText("%10.3f" % val[1])
        self.RBValue[name].setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.RBValue[name].setFixedWidth(70)
        self.savebutton[name]   = QtGui.QPushButton("Save",self)
        self.savebutton[name].setObjectName(name)
        self.savebutton[name].clicked.connect(self.save)
        self.recallbutton[name] = QtGui.QPushButton("Recall",self)
        self.recallbutton[name].setObjectName(name)
        self.recallbutton[name].clicked.connect(self.recall)
        self.deletebutton[name] = QtGui.QPushButton("Delete",self)
        self.deletebutton[name].setObjectName(name)
        self.deletebutton[name].clicked.connect(self.delete)

        layout.addWidget(self.SetValue[name])
        layout.addWidget(self.RBValue[name])
        layout.addWidget(self.savebutton[name])
        layout.addWidget(self.recallbutton[name])
        layout.addWidget(self.deletebutton[name])
        self.group[name].setLayout(layout)
        if (val[2] != 2):
           self.group[name].setEnabled(False)

        self.MagnetFrame.addWidget(self.group[name])

        self.saveval[name]=self.SetValue[name].value()


    def writeSV(self):
        name=self.sender().objectName()
        channel='%s:I-SET' % name
        val=self.sender().value()
#        print(channel,val)
        self.cafe.set(channel,val)

#-----------------
#  timer
    def QTimerEvent(self):
        for name in self.SetValue.keys():
           PVS=[]
           PVS.append("%s:I-SET" % name)
           PVS.append("%s:I-READ" % name)
           PVS.append("%s:PS-MODE" % name)  # should be 2 for being on
           val,stat,statL=self.cafe.getScalarList(PVS,'float')
           if statL[0] is self.cyca.ICAFE_NORMAL:
               self.SetValue[name].blockSignals(True)
               self.SetValue[name].setValue(val[0])
               self.SetValue[name].blockSignals(False)
           if statL[1] is self.cyca.ICAFE_NORMAL:
               self.RBValue[name].setText("%10.3f" % val[1])
           if statL[2] is self.cyca.ICAFE_NORMAL:
               enabled=False
               if val[2]==2:
                   enabled=True
               self.group[name].setEnabled(enabled)    


#----------------
#  read system values from the machine


    def getMagnetSettings(self,name):    
        PVS=[]
        PVS.append("%s:I-SET" % name)
        PVS.append("%s:I-READ" % name)
        PVS.append("%s:PS-MODE" % name)  # should be 2 for being on
        val,stat,statL=self.cafe.getScalarList(PVS,'float')
        for j in range(0,3): 
            if statL[j] is not self.cyca.ICAFE_NORMAL:
                return None
        return val    

#---------------------------------
# 
    def save(self):
        name=str(self.sender().objectName())
        self.saveval[name]=self.SetValue[name].value()
        

    def recall(self):
        name=str(self.sender().objectName())
        self.SetValue[name].setValue(self.saveval[name])


    def delete(self):
        name=str(self.sender().objectName())
        self.MagnetFrame.removeWidget(self.group[name])
        sip.delete(self.group[name])
        self.group[name]=None
        del self.group[name]
        del self.savebutton[name]
        del self.recallbutton[name]
        del self.deletebutton[name]
        del self.SetValue[name]
        del self.RBValue[name]
        del self.saveval[name]


    def DoSaveAll(self):
        for name in self.saveval.keys():
            self.saveval[name]=self.SetValue[name].value()

    def DoRestoreAll(self):        
        for name in self.saveval.keys():
            self.SetValue[name].setValue(self.saveval[name])

    def GoInHibernation(self):
        self.timer.stop()

        for name in self.saveval.keys():
            self.MagnetFrame.removeWidget(self.group[name])
            sip.delete(self.group[name])

        self.group.clear()
        self.savebutton.clear()
        self.recallbutton.clear()
        self.deletebutton.clear()
        self.SetValue.clear()
        self.RBValue.clear()
        self.saveval.clear()
        self.hide()


#---------------
#  spin box overwrite
class custom_spinbox(QtWidgets.QDoubleSpinBox):
    def __init__(self, arg=None):
        super(custom_spinbox, self).__init__(arg)

    def stepBy(self, step):
        value=self.value()
        text=str(self.cleanText())
        idx=self.lineEdit().cursorPosition()
        if (idx==0) and text[0]=='-':
            idx=idx+1
        il=len(text)
        ipos=il-idx
        if ipos>3:
            ipos=ipos-1
        inc=0.001*(10**ipos)*step
        value=value+inc
        self.setValue(value)

# --------------------------------
# Main routine


if __name__ == '__main__':
    app=QtWidgets.QApplication(sys.argv)
    main=DynMagnet()
    main.show()
    sys.exit(app.exec_())
