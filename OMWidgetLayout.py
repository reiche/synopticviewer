#!/usr/bin/env python
import math
import pickle

from epics import caget

from PyQt5 import QtGui,QtCore,QtWidgets



class WidgetLayout:    
# any calculation for the explicit layout of the machine    

    def __init__(self):
        self.x=30
        self.y=180
        self.yinc=0
        self.sectionx=self.x
        self.bendoffset=70
        self.benddirection=0
        self.canvas=None
        self.magnet={}
        self.rf={}
        self.und={}
        self.diag={}
        self.magnetcoord=QtGui.QPolygon()
        self.magnetcoord.append(QtCore.QPoint(self.x-40,self.y))
        try:
            with open('/sf/data/applications/BD-SynView/zpos.pickle','rb') as f:
                self.zposdb=pickle.load(f)
        except OSError:
            print('Cannot find z position cache file')
            self.zposdb={}


    def init(self,incanvas,ystart=150,yinc=0):
        self.x=30
        self.y=ystart
        self.yinc=yinc
        self.sectionx=self.x
        self.canvas=incanvas
        self.benddirection=0
        self.magnetcoord.clear()
        self.magnetcoord.append(QtCore.QPoint(self.x-40,self.y))
        self.tabstart=None
        self.tabend=None

    def save_zpos(self):        # dump the zposdb into a file
        with open('/sf/data/applications/BD-SynView/zpos.pickle','wb') as f:
            pickle.dump(self.zposdb,f)

    def shiftElements(self):
        self.shiftGroup(self.magnet)
        self.shiftGroup(self.rf)
        self.shiftGroup(self.und)
        self.shiftGroup(self.diag)

    def shiftGroup(self,group):
        for ele in group.keys():
            if 'SINBC02' in ele or 'S10BC02' in ele or 'SARCL02' in ele or 'SINLH02' in ele:
                if 'MBND' in ele:
                    continue
                idx=int(ele[12:13])
                if idx==0:
                    continue
                # find upstream and downstream dipole
                iup=0
                idown=0
                for i in range(1,9):
                    test=ele[0:7]+'-MBND'+('%s' % i)+'00'
                    if i<=idx and test in self.magnet.keys():
                        iup=i
                    if i>idx and test in self.magnet.keys():
                        idown=i
                        break

                if iup >0 and idown >0:
                    pos1=self.magnet[ele[0:7]+'-MBND'+('%s' % iup)+'00'].geometry()
                    pos2=self.magnet[ele[0:7]+'-MBND'+('%s' % idown)+'00'].geometry()
                    pos3=group[ele].geometry()
                    dx=pos2.center().x()-pos1.center().x()
                    dy=pos2.center().y()-pos1.center().y()
                    x=pos3.center().x()-pos1.center().x()
                    ddy=int(float(dy*x)/float(dx))+pos1.center().y()-self.y
                    group[ele].move(pos3.left(),pos3.top()+ddy)
        return

    
#----------------------------------------
# sepcific elements


    def getPosition(self,name):
        if name in self.zposdb.keys():
            val=self.zposdb[name]
        else:
            val=caget('%s:Z-POS' % name,timeout=0.1)
            self.zposdb[name]=val

        if val is None:
            return name
        else:
            if self.tabstart is None:
                self.tabstart=val
            self.tabend=val
            return '%s @ %s m' % (name,val)



    def isType(self,name):
        if (name.find('layout')>-1):
            return 1
        else:
            return 0

    def writeRF(self,ele):
      Name=ele.Name.replace('.','-')  
      if 'RGUN' in Name:
          dx=30
          dy=30
          iconpath='./icons/Icon--GunRF.png'

      elif ele.Band=='S':
          dx=80
          dy=20
          iconpath='./icons/Icon-S-BandRF.png'
      elif ele.Band=='X':
          dx=30
          dy=20
          iconpath='./icons/Icon-X-BandRF.png'
      elif ele.Band=='C':
          dx=30
          dy=20
          iconpath='./icons/Icon-X-BandRF.png'

      else:
          return
      self.rf[Name] = QtWidgets.QPushButton("",self.canvas)
      self.rf[Name].setObjectName(Name)
      self.rf[Name].resize(QtCore.QSize(dx,dy))
      self.rf[Name].setIcon(QtGui.QIcon(iconpath));
      self.rf[Name].setIconSize(QtCore.QSize(dx,dy));
      self.rf[Name].move(self.x,self.y-0.5*dy)
      self.rf[Name].setFlat(True)  
      toolname=self.getPosition(Name)
        
      self.rf[Name].setToolTip(toolname)
      self.rf[Name].setStyleSheet("""QToolTip {background-color:white}""")




      self.x=self.x+dx+5
      self.y=self.y+self.yinc


    def writeQuadrupole(self,ele):
      Name=ele.Name.replace('.','-')  
      dx=20
      dy=50
      tilted=False
      if ele.Tilt !=0:
          tilted=True
      self.magnet[Name] = QtWidgets.QPushButton("",self.canvas)
      self.magnet[Name].setObjectName(Name)
      self.magnet[Name].resize(QtCore.QSize(dx,dy))
      if tilted:
          self.magnet[Name].setIcon(QtGui.QIcon("./icons/Icon-SkewQuad.png"));
      else:
          self.magnet[Name].setIcon(QtGui.QIcon("./icons/Icon-Quad.png"));
      self.magnet[Name].setIconSize(QtCore.QSize(dx,dy));
      self.magnet[Name].move(self.x,self.y-0.5*dy)
      self.magnet[Name].setFlat(True)
      toolname=self.getPosition(Name)
      self.magnet[Name].setToolTip(toolname)
      self.magnet[Name].setStyleSheet("""QToolTip {background-color:white}""")

      ddy=40
      dy=20
      dx=20
      if 'corx' in ele.__dict__.keys():
          NameRep=Name.replace(Name[9:12],'CRX')
          self.magnet[NameRep] = QtWidgets.QPushButton("",self.canvas)
          self.magnet[NameRep].setObjectName(NameRep)
          self.magnet[NameRep].resize(QtCore.QSize(dx,dy))
          self.magnet[NameRep].setIcon(QtGui.QIcon("./icons/Icon-CorX.png"));
          self.magnet[NameRep].setIconSize(QtCore.QSize(dx,dy));
          self.magnet[NameRep].move(self.x,self.y-0.5*dy-ddy)
          self.magnet[NameRep].setFlat(True)
          toolname=self.getPosition(NameRep)
          self.magnet[NameRep].setToolTip(toolname)
          self.magnet[NameRep].setStyleSheet("""QToolTip {background-color:white}""")

          
      if 'cory' in ele.__dict__.keys():
          NameRep=Name.replace(Name[9:12],'CRY')
          self.magnet[NameRep] = QtWidgets.QPushButton("",self.canvas)
          self.magnet[NameRep].setObjectName(NameRep)
          self.magnet[NameRep].resize(QtCore.QSize(dx,dy))
          self.magnet[NameRep].setIcon(QtGui.QIcon("./icons/Icon-CorY.png"));
          self.magnet[NameRep].setIconSize(QtCore.QSize(dx,dy));
          self.magnet[NameRep].move(self.x,self.y-0.5*dy+ddy)
          self.magnet[NameRep].setFlat(True)   
          toolname=self.getPosition(NameRep)
          self.magnet[NameRep].setToolTip(toolname)
          self.magnet[NameRep].setStyleSheet("""QToolTip {background-color:white}""")

      self.x=self.x+dx+5
      self.y=self.y+self.yinc


    def writeBend(self,ele):
      Name=ele.Name.replace('.','-')  
      dx=40
      dy=30
      y=self.y
      if 'SINBC' in Name or 'S10BC' in Name or 'SINLH' in Name or 'SARCL' in Name or 'SARUN' in Name or 'SATUN' in Name or 'SATMA01-MBND' in Name:
          if self.benddirection > 0 :
              y=self.y-self.bendoffset
          elif self.benddirection < 0:
              y=self.y
          elif '100' in Name or 'SATMA01-MBND300' in Name:
              y=self.y
          else:
              y=self.y-self.bendoffset
          self.benddirection=self.benddirection-ele.design_angle/abs(ele.design_angle)

      self.magnet[Name] = QtWidgets.QPushButton("",self.canvas)
      self.magnet[Name].setObjectName(Name)
      self.magnet[Name].resize(QtCore.QSize(dx,dy))
      self.magnet[Name].setIcon(QtGui.QIcon("./icons/Icon-Bend.png"));
      self.magnet[Name].setIconSize(QtCore.QSize(dx,dy));
      self.magnet[Name].move(self.x,y-0.5*dy)
      self.magnet[Name].setFlat(True)
      toolname=self.getPosition(Name)

      self.magnet[Name].setToolTip(toolname)
      self.magnet[Name].setStyleSheet("""QToolTip {background-color:white}""")

      self.magnetcoord.append(QtCore.QPoint(self.x,y))

      self.x=self.x+dx+5
      self.y=self.y+self.yinc

      if 'SATSY01' in Name:
          self.yinc=self.yinc-2
      if 'SATCL01' in Name:
          self.yinc=self.yinc+3

    def writeCorrector(self,ele):
      Name=ele.Name.replace('.','-')  

      dx=20
      dy=20
      channel=0;
      if 'corx' in ele.__dict__.keys():
          channel=channel+1
      if 'cory' in ele.__dict__.keys():
          channel=channel+1
    
      if channel > 1:
          ddy=12
      else:
          ddy=0

      if 'corx' in ele.__dict__.keys():
          if 'MCOR' in Name:
              NameRep=Name.replace(Name[9:12],'CRX')
          else:
              NameRep=Name
          self.magnet[NameRep] = QtWidgets.QPushButton("",self.canvas)
          self.magnet[NameRep].setObjectName(NameRep)
          self.magnet[NameRep].resize(QtCore.QSize(dx,dy))
          self.magnet[NameRep].setIcon(QtGui.QIcon("./icons/Icon-CorX.png"));
          self.magnet[NameRep].setIconSize(QtCore.QSize(dx,dy));
          self.magnet[NameRep].move(self.x,self.y-0.5*dy-ddy)
          self.magnet[NameRep].setFlat(True)
          toolname=self.getPosition(NameRep)
          self.magnet[NameRep].setToolTip(toolname)
          self.magnet[NameRep].setStyleSheet("""QToolTip {background-color:white}""")

      if 'cory' in ele.__dict__.keys():
          if 'MCOR' in Name:
              NameRep=Name.replace(Name[9:12],'CRY')
          else:
              NameRep=Name
          self.magnet[NameRep] = QtWidgets.QPushButton("",self.canvas)
          self.magnet[NameRep].setObjectName(NameRep)
          self.magnet[NameRep].resize(QtCore.QSize(dx,dy))
          self.magnet[NameRep].setIcon(QtGui.QIcon("./icons/Icon-CorY.png"));
          self.magnet[NameRep].setIconSize(QtCore.QSize(dx,dy));
          self.magnet[NameRep].move(self.x,self.y-0.5*dy+ddy)
          self.magnet[NameRep].setFlat(True)
          toolname=self.getPosition(NameRep)
          self.magnet[NameRep].setToolTip(toolname)
          self.magnet[NameRep].setStyleSheet("""QToolTip {background-color:white}""")

      self.x=self.x+dx+5
      self.y=self.y+self.yinc

   
    def writeUndulator(self,ele):
      Name=ele.Name.replace('.','-')  
      dx=60
      dy=50
      iconpath='./icons/Icon-Undulator.png'
      if 'UPHS' in Name or 'UDLY' in Name:
          dx=20
          dy=20
          iconpath='./icons/Icon-UndulatorPS.png'
      if 'UMOD' in Name:
          dx=20
          dy=50
          iconpath='./icons/Icon-Modulator.png'
      if 'UDCP' in Name:
          dx=60
          dy=50
          iconpath='./icons/Icon-Dechirper.png'


      self.und[Name] = QtWidgets.QPushButton("",self.canvas)
      self.und[Name].setObjectName(Name)
      self.und[Name].resize(QtCore.QSize(dx,dy))
      self.und[Name].setIcon(QtGui.QIcon(iconpath));
      self.und[Name].setIconSize(QtCore.QSize(dx,dy));
      self.und[Name].setFlat(True)

      self.und[Name].move(self.x,self.y-0.5*dy)
      toolname=self.getPosition(Name)
      self.und[Name].setToolTip(toolname)
      self.und[Name].setStyleSheet("""QToolTip {background-color:white}""")

      self.x=self.x+dx+5
      self.y=self.y+self.yinc

    def writeDiagnostic(self,ele):
      Name=ele.Name.replace('.','-')  
      dx=20
      dy=20
      iconpath=''
      if 'DBPM' in Name:
          iconpath='./icons/Icon-Diagnostic-BPM.png'
      if 'DSCR' in Name:
          iconpath='./icons/Icon-Diagnostic-Screen.png'
      if 'DWSC' in Name:
          iconpath='./icons/Icon-Diagnostic-WSC.png'
      if 'DBAM' in Name:
          iconpath='./icons/Icon-Diagnostic-BAM.png'
      if 'DICT' in Name:
          iconpath='./icons/Icon-Diagnostic-ICT.png'
      if 'DCOL' in Name or 'VCOL' in Name:
          iconpath='./icons/Icon-Diagnostic-COL.png'
      if 'DALA' in Name:
          iconpath='./icons/Icon-Diagnostic-ALA.png'
      if 'DWCM' in Name:
          iconpath='./icons/Icon-Diagnostic-WCM.png'
      if 'DFCP' in Name:
          iconpath='./icons/Icon-Diagnostic-FCP.png'
      if 'DSRM' in Name:
          iconpath='./icons/Icon-Diagnostic-SRM.png'
      if 'DBCM' in Name:
          iconpath='./icons/Icon-Diagnostic-BCM.png'
      if 'DLAC' in Name:
          iconpath='./icons/Icon-Diagnostic-LAC.png'
      if 'DCDR' in Name:
          iconpath='./icons/Icon-Diagnostic-CDR.png'
      if 'DSFH' in Name or 'DSFV' in Name or 'DHVS' in Name:
          iconpath='./icons/Icon-Diagnostic-Slit.png'

      self.diag[Name] = QtWidgets.QPushButton("",self.canvas)
      self.diag[Name].setObjectName(Name)
      self.diag[Name].resize(QtCore.QSize(dx,dy))
      if len(iconpath) > 5:
          self.diag[Name].setIcon(QtGui.QIcon(iconpath));
          self.diag[Name].setIconSize(QtCore.QSize(dx,dy));
          self.diag[Name].setFlat(True)
      toolname=self.getPosition(Name)

      self.diag[Name].setToolTip(toolname)
      self.diag[Name].setStyleSheet("""QToolTip {background-color:white}""")

      self.diag[Name].move(self.x,self.y-0.5*dy)

#      if 'DBPM' in Name:
#          test=QtWidgets.QProgressBar(self.canvas)
#          test.setRange(0,250)
#          test.setValue(100)
#          test.setGeometry(QtCore.QRect(self.x, self.y+200, 10, 100))                 # coordinates position
#          test.setOrientation(QtCore.Qt.Vertical)                       # orientation Horizontal
#          test.setAlignment(QtCore.Qt.AlignCenter)   
#          test.setTextVisible(False)    

      self.x=self.x+dx+5
      self.y=self.y+self.yinc
    

    def writeSextupole(self,ele):
      Name=ele.Name.replace('.','-')  
      dx=20
      dy=50
      self.magnet[Name] = QtWidgets.QPushButton("",self.canvas)
      self.magnet[Name].setObjectName(Name)
      self.magnet[Name].resize(QtCore.QSize(dx,dy))
      self.magnet[Name].move(self.x,self.y-0.5*dy)
      self.magnet[Name].setIcon(QtGui.QIcon("./icons/Icon-Sext.png"));
      self.magnet[Name].setIconSize(QtCore.QSize(dx,dy));
      self.magnet[Name].setFlat(True)
      toolname=self.getPosition(Name)

      self.magnet[Name].setToolTip(toolname)
      self.magnet[Name].setStyleSheet("""QToolTip {background-color:white}""")


      self.x=self.x+dx+5
      self.y=self.y+self.yinc

    def writeVacuum(self,ele):
        Name=ele.Name.replace('.','-')  
        dx=40
        dy=40
        if 'SSTP' in Name or 'SLOS' in Name or 'SDMP' in Name:
            but= QtWidgets.QPushButton("",self.canvas)
            but.setObjectName(Name)
            but.resize(QtCore.QSize(dx,dy))
            but.move(self.x,self.y-0.5*dy)
            but.setIcon(QtGui.QIcon("./icons/Icon-Dump.png"));
            but.setIconSize(QtCore.QSize(dx,dy));
            but.setFlat(True)
            but.setEnabled(False)
            self.magnetcoord.append(QtCore.QPoint(self.x,self.y))
            toolname=self.getPosition(Name)
            but.setToolTip(toolname)
            but.setStyleSheet("""QToolTip {background-color:white}""")

        elif 'VCOL' in Name:
            dx=20
            dy=20
            but = QtWidgets.QPushButton("",self.canvas)
            but.setObjectName(Name)
            but.resize(QtCore.QSize(dx,dy))
            but.setIcon(QtGui.QIcon('./icons/Icon-Diagnostic-COL.png'));
            but.setIconSize(QtCore.QSize(dx,dy));
            but.setFlat(True)
            toolname=self.getPosition(Name)

            but.setToolTip(toolame)
            but.setStyleSheet("""QToolTip {background-color:white}""")
            but.move(self.x,self.y-0.5*dy)

        self.x=self.x+dx+5
        self.y=self.y+self.yinc

        return
    
    def writeSolenoid(self,ele):
        Name=ele.Name.replace('.','-')  
        dx=15
        dy=35
        if 'MSOL11' in Name:
            self.x=self.x-82
        self.magnet[Name] = QtWidgets.QPushButton("",self.canvas)
        self.magnet[Name].setObjectName(Name)
        self.magnet[Name].resize(QtCore.QSize(dx,dy))
        self.magnet[Name].move(self.x,self.y-0.5*dy)
        self.magnet[Name].setIcon(QtGui.QIcon("./icons/Icon-Solenoid.png"));
        self.magnet[Name].setIconSize(QtCore.QSize(dx,dy));
        self.magnet[Name].setFlat(True)
        toolname=self.getPosition(Name)

        self.magnet[Name].setToolTip(toolname)
        self.magnet[Name].setStyleSheet("""QToolTip {background-color:white}""")

        self.x=self.x+dx+5
        self.y=self.y+self.yinc
        return


    def writeLine(self,ele,seq):
        if len(ele.Name)==7:

            label = QtWidgets.QLabel(self.canvas)
            pixmap = QtGui.QPixmap('./icons/Icon-Line.png')
            label.setPixmap(pixmap)
            if self.y < 200 or self.yinc != 0:
               label.move(self.sectionx-4,self.y-140)
            else:   
               label.move(self.sectionx-4,self.y-110)

            label=QtWidgets.QLabel(self.canvas)
            label.setText(ele.Name)
            w=label.geometry().width()
            x=0.5*(self.sectionx+self.x-w/2)
            if x <self.sectionx:
                x=self.sectionx+2
            if self.y < 200 or self.yinc !=0:
                label.move(x,self.y-140)
            else:
                label.move(x,self.y+40)


            self.sectionx=self.x

        return

    def writeDrift(self,s):
        return

    def writeAlignment(self,ele):
        return

    def writeMarker(self,ele):
        return




   
    

