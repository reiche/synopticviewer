import sys

import subprocess

#set search path to include online model. This is hard coded and should be avoided, but it has to do it for now.
sys.path.append('/sf/bd/applications/OnlineModel/current')
sys.path.append('/sf/bd/applications/OnlineModel/current/Layout')


from OMFacility import *
from OMWidgetLayout import *

from PyQt5 import QtGui,QtCore,QtWidgets, uic
from PyQt5.uic import loadUiType
from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtCore import *




from DynMagnet import *

Ui_SynViewGUI, QMainWindow = loadUiType('SynViewGUI.ui')

class SynViewMain(QMainWindow,Ui_SynViewGUI):
    def __init__(self,):
        super(SynViewMain,self).__init__()
        self.setupUi(self)
        self.setStyleSheet("QToolTip {background-color:rgba(0,0,0,255)}") 
#        self.setStyleSheet("QMainWindow {background:transparent;}")
#        self.setStyleSheet("QWidget {background:transparent;}")
#        self.setStyleSheet("QTabWidet,QTabBar {background:transparent; border: 0px; }")
#        tabPalette = QtGui.QPalette()
#         tabPalette.setColor(QtGui.QPalette.Background, QtGui.QColor(0,0,0,255))
#        tab.setAutoFillBackground(True)
#        tab.setPalette(tabPalette)

#        self.AllTabs.setWindowFlags(QtCore.Qt.FramelessWindowHint)
#        self.AllTabs.setAttribute(QtCore.Qt.WA_TranslucentBackground)
#        self.AllTabs.setStyleSheet("Background-color:transparent;")



        self.Facility=Facility(1,0,'10.3.2') # initialize the lattice
        self.Layout=WidgetLayout()
        self.poly=[]
        self.polyalt=[]

        self.constructPanel(self.tabGun,[2,4],'SINEG01','SINSB03',self.Lab1)
        self.constructPanel(self.tabInjector,[2],'SINSB04','SINDI02',self.Lab2)
        self.constructPanel(self.tabLinac1,[2,5],'S10CB01','S10CB07',self.Lab3)
        self.constructPanel(self.tabLinac2,[2],'S10CB08','S10MA01',self.Lab4)
        self.constructPanel(self.tabLinac3,[2],'S20CB01','S20SY03',self.Lab5)
        self.constructPanel(self.tabLinac4,[2],'S30CB01','S30CB09',self.Lab6)
        self.constructPanel(self.tabLinac5,[2],'S30CB10','SARCL01',self.Lab7)
        self.constructPanel(self.tabAramis1,[2],'SARCL02','SARUN02',self.Lab8)
        self.constructPanel(self.tabAramis2,[2],'SARUN03','SARUN10',self.Lab9)
        self.constructPanel(self.tabAramis3,[2,6],'SARUN11','SARBD01',self.Lab10)
        self.constructPanel(self.tabAthos1,[3],'SATSY01','SATCL01',self.Lab11,400,-2)
        self.constructPanel(self.tabAthos2,[3],'SATDI01','SATCL02',self.Lab12)
        self.constructPanel(self.tabAthos3,[3],'SATMA01','SATUN06',self.Lab13)
        self.constructPanel(self.tabAthos4,[3],'SATUN07','SATUN14',self.Lab14)
        self.constructPanel(self.tabAthos5,[3],'SATUN15','SATUN22',self.Lab15)
        self.constructPanel(self.tabAthos6,[3,7],'SATMA02','SATBD01',self.Lab16)

        self.Layout.shiftElements()
        self.AllTabs.setPoly(self.poly,self.polyalt)
#        self.Layout.save_zpos()

        # now connect all buttons to the event handler
        for ele in self.Layout.diag.keys():
            self.Layout.diag[ele].clicked.connect(self.startDiag)

        # now connect all buttons to the event handler
        for ele in self.Layout.rf.keys():
            self.Layout.rf[ele].clicked.connect(self.startRF)

        for ele in self.Layout.magnet.keys():
            self.Layout.magnet[ele].clicked.connect(self.startMagnet)
         
#        self.magnetPanel=DynMagnet()  



    def constructPanel(self,tab,inline,start,end,textlabel,ystart=180,inc=0):

        self.Layout.init(tab,ystart-30,inc)                   # defines the tab where to place elements 
        line=self.Facility.BeamPath(inline[0])  # select the line
        line.setRange(start,end)           # start from laser heater
        line.writeLattice(self.Layout,self.Facility)
        label='Z range: %s - %s m' % (self.Layout.tabstart,self.Layout.tabend)
        textlabel.setText(label)
        self.Layout.magnetcoord.append(QtCore.QPoint(self.geometry().right()-100,self.Layout.y))
        self.poly.append(QtGui.QPolygon())
        self.polyalt.append(QtGui.QPolygon())
        for ele in self.Layout.magnetcoord:
            self.poly[-1].append(QtCore.QPoint(ele.x()+20,ele.y()+27))


        if len(inline)>1:
            self.Layout.y=280
            line=self.Facility.BeamPath(inline[1])
            if inline[1]==4:
                self.Layout.x=self.Layout.magnet['SINEG01-MBND300'].geometry().right()+15
                self.Layout.sectionx=self.Layout.x-9
                sec=end[0:3]+'BD01'
            elif inline[1]==5:
                self.Layout.x=self.Layout.magnet['S10DI01-MBND100'].geometry().right()+15
                self.Layout.sectionx=self.Layout.x-9
                sec=end[0:3]+'BD01'
            elif inline[1]==6:
                self.Layout.x=self.Layout.magnet['SARBD01-MBND100'].geometry().right()+15
                self.Layout.sectionx=self.Layout.x-9
                sec=end[0:3]+'BD02'
            else:
                self.Layout.x=self.Layout.magnet['SATBD01-MBND200'].geometry().right()+15
                self.Layout.sectionx=self.Layout.x-9
                sec=end[0:3]+'BD02'
            xstart=self.Layout.x
            line.setRange(sec,sec)           # start from laser heater
            line.writeLattice(self.Layout,self.Facility)
            self.Layout.y=150
            self.polyalt[-1].append(QtCore.QPoint(self.Layout.magnetcoord[0].x()+30,self.Layout.magnetcoord[0].y()+27))
            self.polyalt[-1].append(QtCore.QPoint(xstart-35,self.Layout.y+27))
            self.polyalt[-1].append(QtCore.QPoint(xstart+15,255+52))
            self.polyalt[-1].append(QtCore.QPoint(self.Layout.magnetcoord[-1].x()+50,255+52))

#---------------------------------
# event handaler
    
    def startDiag(self):        
        name=self.sender().objectName()
        expert=QtGui.qApp.keyboardModifiers() & QtCore.Qt.ControlModifier    

        if 'DSCR' in name:
            command = ["screen_panel","-cam=%s" % name]
            p=subprocess.Popen(command)

    def startRF(self):
        name=self.sender().objectName()

        BPM="BPM=SARCL02-DBPM110"
        if 'S10' in name:
            BPM="BPM=S10BC02-DBPM140"
        if 'SAT' in name:
            BPM="BPM=SATDI01-DBPM210"
        if 'SIN' in name:
            BPM="BPM=SINBC02-DBPM140"
        if 'SINEG' in name:
            BPM="GUN=1"
        if 'SINDI' in name:
            BPM="BPM=S10BD01-DBPM020"

        expert=QtGui.qApp.keyboardModifiers() & QtCore.Qt.ControlModifier    

        command=["caqtdm", "-stylefile","sfop.qss","-macro","RF_station=%s,%s,ARC=ARCYES" % (name[0:7],BPM), "S_LLRF-RF-STATION.ui"]
        print(command)
        if (expert):
            command=["caqtdm","-stylefile","sfop.qss","-macro","RF_station=%s,%s" % (name[0:7],BPM), "S_LLRF_rfstation_expert_main.ui"]

        p=subprocess.Popen(command)

    def startMagnet(self):
        return
        export=QtGui.qApp.keyboardModifiers() & QtCore.Qt.ControlModifier
        self.magnetPanel.addMagnet(self.sender().objectName())
        self.magnetPanel.show()


#    def paintEvent(self, event):
#        painter = QtGui.QPainter(self)
#        painter.setRenderHint(QtGui.QPainter.Antialiasing)
#
#        itab=self.AllTabs.currentIndex()
#        ilen=len(self.poly[itab])
#        for i in range(0,ilen-1):
#            painter.drawLine(self.poly[itab][i],self.poly[itab][i+1])
#        ilen=len(self.polyalt[itab])
#        for i in range(0,ilen-1):
#            painter.drawLine(self.polyalt[itab][i],self.polyalt[itab][i+1])
        
    def closeEvent(self, event):
#        self.magnetPanel.closeEvent(event)
        event.accept()


# -------------------------------- 
# Main routine


if __name__ == '__main__':
    QtWidgets.QApplication.setStyle(QtWidgets.QStyleFactory.create("plastique"))
    app=QtWidgets.QApplication(sys.argv)
    main=SynViewMain()
    main.show()
    sys.exit(app.exec_())
